import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import "./App.scss";
import Categories from "./components/categories";
import Sidebar from "./components/sidebar";
import { getAllCategories } from "./redux/categoryAction";

function App() {
  const dispatch = useDispatch();
  const categories = useSelector((state) => state.categories.categories);
  
  const [categoryId, setCategoryId] = useState(null);
  const [limit, setLimit] = useState(10);

  useEffect(() => {
    if (categories === undefined || categories.length == 0) dispatch(getAllCategories());
  }, []);

  return (
    <div className="App">
      <Sidebar categories={categories} categoryId={categoryId} setCategoryId={setCategoryId} setLimit={setLimit} />
      <Categories categoryId={categoryId} limit={limit} setLimit={setLimit} />
    </div>
  );
}

export default App;
