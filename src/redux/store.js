import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import allReducers from "./reducers";

const initialState = {};

const middleware = [thunk];

let composeWithDevTools;

if (window.__REDUX_DEVTOOLS_EXTENSION__) {
  composeWithDevTools = compose(
    applyMiddleware(
      ...middleware,
    ),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  );
}

const store = createStore(allReducers, initialState, composeWithDevTools);

export default store;
