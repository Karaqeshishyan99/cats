import axios from 'axios';
import { GET_ALL_CATEGORIES, GET_CATEGORY } from './actionTypes';

// Get all categories
export const getAllCategories = () => (dispatch) => {
    axios
        .get('https://api.thecatapi.com/v1/categories')
        .then((res) => {
            console.log(res, 'res');
            dispatch({
                type: GET_ALL_CATEGORIES,
                payload: res.data,
            });
        })
        .catch((err) => {
            if (err.response && err.response.data) {
                const { data } = err.response;
                dispatch({
                    payload: {
                        message: data.message ? (data.message.message ? data.message.message : data.message) : "Unknown error"
                    }
                });
            }
        });
};

// Get category by id or page
export const getCategory = (id, limit, page) => (dispatch) => {
    axios
        .get(`https://api.thecatapi.com/v1/images/search?limit=${limit ? limit : 10}&page=${page ? page : 1}&category_ids=${id}`)
        .then((res) => {
            console.log(res, 'res category');
            dispatch({
                type: GET_CATEGORY,
                payload: res.data,
            });
        })
        .catch((err) => {
            if (err.response && err.response.data) {
                const { data } = err.response;
                dispatch({
                    payload: {
                        message: data.message ? (data.message.message ? data.message.message : data.message) : "Unknown error"
                    }
                });
            }
        });
};