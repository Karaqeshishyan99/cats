import { GET_ALL_CATEGORIES, GET_CATEGORY } from "./actionTypes";

const initialState = {
    category: [],
    categories: [],
};

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_ALL_CATEGORIES:
            return {
                ...state,
                categories: action.payload,
            };
        case GET_CATEGORY:
            return {
                ...state,
                category: action.payload,
            };
        default:
            return state;
    }
}
