import { useDispatch, useSelector } from "react-redux";
import { getCategory } from "../redux/categoryAction";

const Sidebar = ({ categories, categoryId, setLimit, setCategoryId }) => {
    return ( 
        <div className="sidebar">
            <div className="categories">
                {categories && categories.map((item, index) => (
                    <CatCategory 
                        key={index} 
                        categoryId={categoryId} 
                        setCategoryId={setCategoryId} 
                        setLimit={setLimit} 
                        id={item.id} 
                        name={item.name} 
                    />
                ))}
            </div>
        </div>
     );
}

const CatCategory = ({categoryId, setCategoryId, setLimit, id, name}) => {
    const dispatch = useDispatch();
    const category = useSelector((state) => state.categories.category);

    const handleClick = () => {
        if (category === undefined || category.length == 0 || categoryId != id) {
            dispatch(getCategory(id));
            setCategoryId(id);
            setLimit(10);
        }
    }

    return (
        <div className="category" onClick={() => handleClick()}> 
            {name}
        </div>
    )
}
 
export default Sidebar;