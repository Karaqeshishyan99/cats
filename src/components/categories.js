import { useDispatch, useSelector } from "react-redux";
import { getCategory } from "../redux/categoryAction";

const Categories = ({ categoryId, limit, setLimit }) => {
    const dispatch = useDispatch();
    const category = useSelector((state) => state.categories.category);

    const handleClick = () => {
        dispatch(getCategory(categoryId, limit + 10));
        setLimit(limit + 10);
    }

    return category === undefined || category.length == 0 ? ( 
        <h1 style={{ textAlign: "center" }}>Choose a category</h1> 
    ) : (
        <div className="cats">
            {category.map((item, index) => (
                <CatImage key={index} width={item.width} height={item.height} url={item.url} />
            ))}
            <button onClick={() => handleClick()}>Load more</button>
        </div>
    );
}

const CatImage = ({ width, height, url }) => {
    return (
        <div className="imgae-box">
            <div style={{ paddingTop: `calc(${height / width * 100}%)` }} className="aspect-ratio-box">
                <img src={url} alt="cat_image" />
            </div>
        </div>
    )
}
 
export default Categories;